import subprocess
import os
import re
import platform
from functools import reduce

fileDirectory = os.path.dirname(os.path.abspath(__file__))
vhdlFileDir = fileDirectory + "/vhdlfiles/"
system = platform.system()

def vhdlTruthTable(functions, studentCode):
	vhdlTableFile = open(vhdlFileDir + str(studentCode) + "_truthTable.vhd", "w+", encoding="utf-8")
	counter = 0

	# Mandatory text blocks.

	begining = ("------------------------------------------------------------------------\n"
	"-- IAY0150 - Homework #1. Truth table of " + str(studentCode) + ".\n"
	"------------------------------------------------------------------------\n"
	"-- (C) Generation programmed by Kaido Siimer, example by Peeter Ellervee\n"
	"------------------------------------------------------------------------\n"
	"library IEEE; use IEEE.std_logic_1164.all;\n"
	"entity f_system is\n"
	"port ( x1, x2, x3, x4: in std_logic;\n"
	"		y1, y2, y3, y4: out std_logic );\n"
	"end entity f_system;\n"
	"\n"
	"library IEEE; use IEEE.std_logic_1164.all;\n"
	"architecture tabel of f_system is\n"
	"begin\n"
	"process (x1, x2, x3, x4)\n"
	"	variable in_word, out_word: std_logic_vector (3 downto 0);\n"
	"begin\n"
	"	in_word := x1 & x2 & x3 & x4;\n"
	"	case  in_word  is\n")

	end = ("        when others => out_word := \"----\";\n"
	"	end case;\n"
	"	y1 <= out_word(3);    y2 <= out_word(2);\n"
	"	y3 <= out_word(1);    y4 <= out_word(0);\n"
	"end process;\n"
	"end architecture tabel;")

	#Generation of truth table into code.

	vhdlTableFile.write(begining)
	for function in functions:
		stringToWrite = '\t\twhen "' + bin(counter)[2:].zfill(4) + '" => out_word := "' + function + '";\n'
		vhdlTableFile.write(stringToWrite)
		counter += 1
	vhdlTableFile.write(end)
	vhdlTableFile.close()
def vhdlEspresso(formulas, studentCode, phase):
	vhdlEspressoFile = open(vhdlFileDir + str(studentCode) + "_espresso.vhd", "w+", encoding="utf-8")
	begining = ("------------------------------------------------------------------------\n"
	"-- IAY0150 - Homework #1. Espresso result of the " + str(studentCode) + ".\n"
	"------------------------------------------------------------------------\n"
	"-- (C) Generation programmed by Kaido Siimer, example by Peeter Ellervee\n"
	"------------------------------------------------------------------------\n"
	"library IEEE; use IEEE.std_logic_1164.all;\n"
	"entity f_system is\n"
	"port ( x1, x2, x3, x4: in std_logic;\n"
	"		y1, y2, y3, y4: out std_logic );\n"
	"end entity f_system;\n"
	"\n"
	"library IEEE; use IEEE.std_logic_1164.all;\n"
	"architecture espresso of f_system is\n"
	"begin\n")
	end = "end architecture espresso;"

	vhdlEspressoFile.write(begining)
	parseFormulas(formulas, phase, vhdlEspressoFile)
	vhdlEspressoFile.write(end)
	vhdlEspressoFile.close()

def optimalPhases(output):
	phases = []
	leastImplicants = []
	leastTotal = []
	bestPhases = []
	phaseCounter = -1
	minImplicants = 0
	minTotal = 0
	for item in output:
		if "opoall" in item:
			break
		if output.index(item) % 2 == 0:
			phases.append([item.split(' ')[4]])
			phaseCounter += 1
		else:
			currentPhaseInfo = item.split('c=')[1]
			implicantCount = int(currentPhaseInfo.split('(')[0])
			inCount = int(currentPhaseInfo.split('in=')[1].split(' ')[0])
			outCount = int(currentPhaseInfo.split('out=')[1].split(' ')[0])
			totalCount = int(currentPhaseInfo.split('tot=')[1])
			phases[phaseCounter].append(implicantCount)
			phases[phaseCounter].append(inCount)
			phases[phaseCounter].append(outCount)
			phases[phaseCounter].append(totalCount)
			if phaseCounter == 0:
				minImplicants = phases[phaseCounter][1]
				minTotal = phases[phaseCounter][4]
			else:
				if minImplicants > phases[phaseCounter][1]:
					minImplicants = phases[phaseCounter][1]
				if minTotal > phases[phaseCounter][4]:
					minTotal = phases[phaseCounter][4]

	for item in phases:
		if item[1] == minImplicants:
			leastImplicants.append(item)
		if item[4] == minTotal:
			leastTotal.append(item)

	matches = 0

	for item in leastTotal:
		if item in leastImplicants:
			bestPhases.append(item[0])
			matches += 1

	if matches == 0:
		for item in leastTotal:
			bestPhases.append(item[0])
	return bestPhases

def calcFunc(code,multiplier, determined, notDetermined):
	out = code
	while(out < 0x10000000):
		out *= multiplier
	for i in range(0,8):
		tempValue = (out & (0xF << i*4)) >> i*4
		if not tempValue in determined:
			determined.append(tempValue)

	out= int(out/3)
	for i in range(0,8):
		tempValue = (out & (0xF << i*4)) >> i*4
		if(not ((i==7) and (tempValue == 0))):
			if (not tempValue in determined) and (not tempValue in notDetermined):
				notDetermined.append(tempValue)

	notDetermined.sort()
	determined.sort()
	return (determined, notDetermined)

def splitkeepsep(s, sep):
    return reduce(lambda acc, elem: acc[:-1] + [acc[-1] + elem] if elem == sep else acc + [elem], re.split("(%s)" % re.escape(sep), s), [])

def printNibble(byte):
	nibble = ""
	for i in range(3,-1,-1):
		nibble += str((byte & (1 << i)) >> i)
	return nibble

def espresso(functionSelection, mode, phase=''):

	outputFile = open(fileDirectory + "/output.txt", "w+", encoding='utf-8')
	outputFile.write(".i 4\n.o 4\n")
	if mode == "phase":
		outputFile.write(".phase " + phase + '\n')
	elif mode == "formula":
		outputFile.write(".ilb x1 x2 x3 x4\n.ob y1 y2 y3 y4\n")
		outputFile.write(".phase " + phase + '\n')

	for num in range(2**4):
		outputFile.write(printNibble(num))
		outputFile.write(" " + str(functionSelection[num]) + '\n')

	outputFile.write('\n.e\n')
	outputFile.close()

	if system == "Linux" or system == "Darwin":
		if mode == "phase":
			command = "./espresso output.txt"
		elif mode == "formula":
			command = "./espresso -o eqntott output.txt"
		else:
			command = "./espresso -Dopoall output.txt"
	else:
		if mode == "phase":
			command = "espresso.exe output.txt"
		elif mode == "formula":
			command = "espresso.exe -o eqntott output.txt"
		else:
			command = "espresso.exe -Dopoall output.txt"


	proccess = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
	output = proccess.communicate()[0]

	if mode == "formula":
		output = output.decode('utf-8').replace("\n", "")
		output = splitkeepsep(output, ";")
	else:
		output = output.decode('utf-8').split('\n')

	return output

def gateCount(solution):
	data = solution.copy()
	andGatesCount = []
	orGatesCount = [0, 0, 0, 0]

	andMaxInput = 0
	orMaxInput = 0

	#clean data for processing
	data.remove(data[len(data) - 1])
	data.remove(data[len(data) - 1])
	data.remove(data[0])
	data.remove(data[0])
	data.remove(data[0])
	data.remove(data[0])

	for item in data:
		splitItem = item.split(" ")
		andGatesCount.append(splitItem[0].count("1") + splitItem[0].count("0"))
		letterCounter = 0
		for letter in splitItem[1]:
			if letter == "1":
				orGatesCount[letterCounter] += 1
			letterCounter +=1

	andMaxInput = max(andGatesCount)
	orMaxInput = max(orGatesCount)
	andMinInput = min(andGatesCount)
	orMinInput = min(orGatesCount)


	function = {
		"andGates" : andGatesCount,
		"orGates" : orGatesCount,
		"andMaxInput" : andMaxInput,
		"andMinInput" : andMinInput,
		"orMaxInput" : orMaxInput,
		"orMinInput" : orMinInput
	}

	return function

def findBestSolution(solutionsData):
	overallAndMax = []
	overallAndMin = []
	overallOrMax = []
	overallOrMin = []
	solutionIndex = []
	for item in solutionsData:
		overallAndMax.append(item['andMaxInput'])
		overallAndMin.append(item['andMinInput'])
		overallOrMax.append(item['orMaxInput'])
		overallOrMin.append(item['orMinInput'])

	overallAndMax = min(overallAndMax)
	overallAndMin = min(overallAndMin)
	overallOrMax = min(overallOrMax)

	for item in solutionsData:
		if item['andMaxInput'] == overallAndMax and item['orMaxInput'] == overallOrMax:
			solutionIndex.append(solutionsData.index(item))

	if len(solutionIndex) == 1:
		solutionIndex = solutionIndex[0]
	else:
		minGateImplicantCount = []
		for item in solutionsData:
			minGateImplicantCount.append(item['andGates'].count(overallAndMin))
		maxCountOfMinAnds = max(minGateImplicantCount)
		return minGateImplicantCount.index(maxCountOfMinAnds)

	return solutionIndex

def parseFormulas(formulas, phase, file):
	#clear empty lines
	while "" in formulas:
		formulas.remove("")

	print("Phase is " + phase + "\nFormulas:")
	for formula in formulas:
		print(formula)
		string = ""
		letterCounter = 0
		for letter in formula:
			if letter == "=":
				string += " <= ("
			elif letter == "!":
				string += "(not "
			elif letter == "y":
				string += "\t" + letter
			elif letter == "x" and formula[letterCounter - 1] != "!":
				string += "(" + letter
			elif letter == "&":
				string += " and "
			elif letter == "|":
				string += ") or \n\t\t\t("
			elif letter == "(" or letter == ")" or letter == " ":
				string += ""
			elif letter.isdigit() and formula[letterCounter - 1] != "y" :
				string += letter + ")"
			elif letter == ";":
				string += ")" + letter
			else:
				string += letter
			letterCounter += 1
		if phase[formulas.index(formula)] == "0":
			string = list(string)
			string.insert(6, " not (")
			string.insert(len(string) - 2, ")")
			string = "".join(string)
		file.write(string + "\n")

def generateFunctions(studentCode):
	functions = []
	multiplierValues = [0x5,0x7,0xB,0xD]
	for i in range(0,4):
		determined = []
		notDetermined = []
		calcFunc(studentCode,multiplierValues[i], determined, notDetermined)
		functions.append([determined,notDetermined])


	functionSelection = []
	for num in range(2**4):
		string = ""
		for func in range(4):
			if num in functions[func][0]: #Determined
				string+='1'
			elif num in functions[func][1]: #Notdetermined
				string+='-'
			else:
				string+="0"
		functionSelection.append(string)
	return functionSelection

def removeSymbols(text):
	text = text.replace(" ", "")
	text = text.replace("(", "")
	text = text.replace(")", "")
	text = text.replace("=", "")
	text = text.replace("y1", "")
	text = text.replace("y2", "")
	text = text.replace("y3", "")
	text = text.replace("y4", "")
	text = text.replace(";", "")
	text = text.replace("\r", "")
	return text

def makeReplacements(text):
	text = text.replace("!x1", "e")
	text = text.replace("!x2", "f")
	text = text.replace("!x3", "g")
	text = text.replace("!x4", "h")
	text = text.replace("x1", "a")
	text = text.replace("x2", "b")
	text = text.replace("x3", "c")
	text = text.replace("x4", "d")
	return text

def findCores(formulas):
	#clear empty lines
	while "" in formulas:
		formulas.remove("")

	formatedFormulas = []
	cores = []

	for formula in formulas:
		formulaCopy = formula.split("|")
		formatedFormulas.append([])
		for item in formulaCopy:
			item = removeSymbols(item)
			item = makeReplacements(item)
			formatedFormulas[formulas.index(formula)].append(item)

	formulaCounter = 0
	for formula in formatedFormulas:
		cores.append([])
		cores[formulaCounter].append({})
		formula = " + ".join(formula)
		print(formula)
		formula = formula.replace("&", "")
		formula = formula.split(" + ")

		# get uniques

		uniques = list(set("".join(formula)))
		formula = "+".join(formula)


		for unique in uniques:
			if formula.count(unique) > 1:
				cores[formulaCounter][0][unique] = []
				for item in formula.split("+"):
					if unique in item:
						cores[formulaCounter][0][unique].append(item.replace(unique, ""))

		formulaCounter += 1

	for formula in cores:
		print(formula)

if __name__ == "__main__":

	# take care of directory
	if not os.path.isdir(vhdlFileDir):
		os.mkdir(vhdlFileDir)

	# give rights to espresso espresso
	if system == "Linux" or system == "Darwin":
		os.chmod(fileDirectory + "/espresso", 0o755)

	# get studentCode

	studentCode = int(input("Enter your student code: "))

	# generate logic functions depentent on student code
	functions = generateFunctions(studentCode)

	# generate vhdl file for functions
	vhdlTruthTable(functions, studentCode)

	# run inital espresso optimation for all solutions
	output = espresso(functions, "initial")

	# find best solutions for optimisation, least implicants and least total literals
	optimalSolutions = optimalPhases(output)

	# print phases for optimal solutions
	solutionsData = []
	for item in optimalSolutions:
		solutionsData.append([])
		solution = espresso(functions, "phase", item)
		solutionsData[optimalSolutions.index(item)] = gateCount(solution)
		for line in solution:
			print(line)

	# find best case
	bestSolutionPhase = optimalSolutions[findBestSolution(solutionsData)]

	# generate formulas for best solution
	formulas = espresso(functions, "formula", bestSolutionPhase)

	# Generate vhdl file for best solution.
	vhdlEspresso(formulas, studentCode, bestSolutionPhase)

	# Find cores of the functions.

	findCores(formulas)

	#clean up helper file
	os.remove(fileDirectory + "/output.txt")

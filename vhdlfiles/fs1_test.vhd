------------------------------------------------------------------------
-- IAY0150 - Homework #1. Test bench for the example task.
------------------------------------------------------------------------
-- (C) Peeter Ellervee - 2016 - Tallinn
-- Modified by Kaido Siimer 2018, changed signal names,
-- for better readability, sort by name, so functions line up.
------------------------------------------------------------------------
library IEEE; use IEEE.std_logic_1164.all;
entity test is
end entity test;

library IEEE; use IEEE.std_logic_1164.all;
architecture bench of test is
  signal x1, x2, x3, x4: std_logic;
  signal ya1, yb1, yc1, ya2, yb2, yc2: std_logic;
  signal ya3, yb3, yc3, ya4, yb4, yc4: std_logic;
  signal yx1, yx2, yx3, yx4: std_logic;
  component f_system
    port ( x1, x2, x3, x4: in std_logic;
           y1, y2, y3, y4: out std_logic );
  end component;
  for U1: f_system use entity work.f_system(tabel);
  for U2: f_system use entity work.f_system(espresso);
  for U3: f_system use entity work.f_system(espresso);

  function compare_signals (s1, s2, s3: std_logic) return std_logic is
  begin
    if  s1='-'  then
      if  s2/=s3  then  return 'X';  end if;
    else
      if  s1/=s2 or s1/=s3  then  return 'X';  end if;
    end if;
    return '0';
  end function compare_signals;
begin
  -- Input signals (after every 10 ns)
  x1 <= '0' after 0 ns, '1' after 80 ns, '0' after 160 ns;
  x2 <= '0' after 0 ns, '1' after 40 ns, '0' after 80 ns, '1' after 120 ns;
  x3 <= '0' after 0 ns, '1' after 20 ns, '0' after 40 ns, '1' after 60 ns,
        '0' after 80 ns, '1' after 100 ns, '0' after 120 ns, '1' after 140 ns;
  x4 <= '0' after 0 ns, '1' after 10 ns, '0' after 20 ns, '1' after 30 ns,
        '0' after 40 ns, '1' after 50 ns, '0' after 60 ns, '1' after 70 ns,
        '0' after 80 ns, '1' after 90 ns, '0' after 100 ns, '1' after 110 ns,
        '0' after 120 ns, '1' after 130 ns, '0' after 140 ns, '1' after 150 ns;

  -- System of Boolean functions
  U1: f_system port map (x1, x2, x3, x4, ya1, ya2, ya3, ya4);
  U2: f_system port map (x1, x2, x3, x4, yb1, yb2, yb3, yb4);
  U3: f_system port map (x1, x2, x3, x4, yc1, yc2, yc3, yc4);
  -- yc1<=yb1;  yc2<=yb2;  yc3<=yb3;  yc4<=yb4;
  yx1 <= compare_signals (ya1, yb1, yc1);
  yx2 <= compare_signals (ya2, yb2, yc2);
  yx3 <= compare_signals (ya3, yb3, yc3);
  yx4 <= compare_signals (ya4, yb4, yc4);
end architecture bench;
